import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;


public class HiltonComTest {
    private static WebDriver driver;
    private static MainPage mainPage;
    private static HotelsPage hotelsPage;
    private static RoomsPage roomsPage;
    private static FormPage formPage;
    private static TotalPrice totalPrice;
    private final static Logger logger = Logger.getLogger(HiltonComTest.class);

    @BeforeClass
    private static void setUp() {
        System.setProperty("webdriver.gecko.driver", HiltonComTest.class.getResource("geckodriver.exe").getPath());
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(240, TimeUnit.SECONDS);
        driver.navigate().to("https://www3.hilton.com/");
        mainPage = new MainPage(driver);
    }

    @Test
    public void testHilton() {
        mainPage.clickAdvancedSearch();
        mainPage.selectNumber(mainPage.numberOfRooms, "1");
        mainPage.selectNumber(mainPage.roomsAdults, "2");
        mainPage.selectNumber(mainPage.roomChildren, "0");
        mainPage.selectHotel("Washington, DC");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy");
        LocalDate startDate = LocalDate.now().plusDays(2);
        LocalDate endDate = startDate.plusDays(7);
        mainPage.selectDate(mainPage.checkin, startDate.format(formatter));
        mainPage.selectDate(mainPage.checkout, endDate.format(formatter));

        hotelsPage = mainPage.clickGo();
        hotelsPage.selectFilterAvailableOnly();
        roomsPage = hotelsPage.selectHotel(1);

        roomsPage.selectRoomWithMaxPrice();

        formPage = roomsPage.selectRoomWithMaxPrice();

        formPage.enterValue(formPage.guestFirstName, "Aleksey");
        formPage.enterValue(formPage.guestLastName, "Ivanov");
        formPage.enterValue(formPage.guestPhone, "80291234567");
        formPage.enterValue(formPage.guestEmail,"awm@bk.ru");
        formPage.selectValue(formPage.guestAddressType,"Home");
        formPage.selectValue(formPage.guestCountry,"Russian Federation");
        formPage.enterValue(formPage.guestGenericAddress1,"Lenina 1-1");
        formPage.enterValue(formPage.guestGenericCity,"Moskow");

        totalPrice = formPage.clickContinue();

        logger.info(totalPrice.getText(totalPrice.hotelName));
        logger.info(totalPrice.getText(totalPrice.hotelAddress));
        logger.info(totalPrice.getText(totalPrice.totalPrice));
    }

    @AfterClass
    public static void tearDown() {
        driver.close();
    }

}
