import com.machinepublishers.jbrowserdriver.JBrowserDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TutByTest {
    public static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        System.setProperty("webdriver.gecko.driver", TutByTest.class.getResource("geckodriver.exe").getPath());
        driver = new FirefoxDriver();
        driver.navigate().to("https://tut.by/");

    }

    @Test
    public void testNashPRZDent() {
        JBrowserDriver jDriver = new JBrowserDriver();
        jDriver.get("https://tut.by/");
        String text2 = jDriver.findElement(By.xpath("//body")).getText();
        String text1 = driver.findElement(By.xpath("//body")).getText();

        int count1 = countOfWords(text1, "Лукашенко");
        int count2 = countOfWords(text2, "Лукашенко");
        Assert.assertEquals(count2, count1, "the counts are different");
    }

    public static int countOfWords(String text, String word){
        int count = 0;
        String[] words = text.split(" ");
        for (int i = 0; i < words.length; i++){
            if (words[i].contains(word)){
                count ++;
            }
        }
        return count;
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }

}
