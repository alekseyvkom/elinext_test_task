package ftp;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class FTPClient {

    private Socket socket = null;
    private BufferedReader reader = null;
    private BufferedWriter writer = null;

    public synchronized void connect(String host, int port, String user,
                                     String pass) throws IOException {
        if (socket != null) {
            throw new IOException("SimpleFTP is already connected. Disconnect first.");
        }
        socket = new Socket(host, port);
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer = new BufferedWriter(
                new OutputStreamWriter(socket.getOutputStream()));

        String response = readLine();
        if (!response.startsWith("220 ")) {
            throw new IOException(
                    "FTPClient received an unknown response when connecting to the ftp server: "
                            + response);
        }

        sendLine("USER " + user);

        response = readLine();
        if (!response.startsWith("331 ")) {
            throw new IOException(
                    "FTPClient received an unknown response after sending the user: "
                            + response);
        }

        sendLine("PASS " + pass);

        response = readLine();
        if (!response.startsWith("230 ")) {
            throw new IOException(
                    "FTPClient was unable to log in with the supplied password: "
                            + response);
        }
    }

    public synchronized void disconnect() throws IOException {
        try {
            sendLine("QUIT");
        } finally {
            socket = null;
        }
    }

    public synchronized boolean mkd(String dirName) throws IOException {
        sendLine("MKD " + dirName);
        String response = readLine();
        return (response.startsWith("257 "));
    }

    public synchronized boolean rmd(String dirName) throws IOException {
        sendLine("RMD " + dirName);
        String response = readLine();
        return (response.startsWith("250 "));
    }

    public synchronized boolean cwd(String dir) throws IOException {
        sendLine("CWD " + dir);
        String response = readLine();
        return (response.startsWith("250 "));
    }

    public synchronized List<String> list() throws IOException {
        sendLine("PASV");
        String response = readLine();
        String ip = null;
        int port = -1;
        int opening = response.indexOf('(');
        int closing = response.indexOf(')', opening + 1);
        if (closing > 0) {
            String dataLink = response.substring(opening + 1, closing);
            StringTokenizer tokenizer = new StringTokenizer(dataLink, ",");
            try {
                ip = tokenizer.nextToken() + "." + tokenizer.nextToken() + "."
                        + tokenizer.nextToken() + "." + tokenizer.nextToken();
                port = Integer.parseInt(tokenizer.nextToken()) * 256
                        + Integer.parseInt(tokenizer.nextToken());
            } catch (Exception e) {
                throw new IOException("FTPClient received bad data link information: "
                        + response);
            }
        }

        Socket dataSocket = new Socket(ip, port);
        sendLine("LIST");
        response = readLine();
        if (response.startsWith("150 ")) {
            response = readLine();
        }
        if (!response.startsWith("226 ")) {
            throw new IOException("FTPClient was not allowed to send the file: "
                    + response);
        }

        BufferedReader dataReader = new BufferedReader(new InputStreamReader(dataSocket.getInputStream()));
        List<String> directories = new ArrayList<>();
        String line;
        while ((line = dataReader.readLine()) != null) {
            System.out.println("< " + line);
            if (line.startsWith("d")) {
                String[] tokens = line.split(" ");
                int size = tokens.length;
                directories.add(tokens[size - 1]);
            }
        }
        return directories;
    }


    private void sendLine(String line) throws IOException {
        writer.write(line + "\r\n");
        writer.flush();
        System.out.println("> " + line);
    }

    private String readLine() throws IOException {
        String line = reader.readLine();
        System.out.println("< " + line);
        return line;
    }
}
//Appache FTP client