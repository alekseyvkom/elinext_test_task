package ftp;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;


public class FTPClientTest {

    static FTPClient client = new FTPClient();

    @BeforeClass
    public static void setUp() throws IOException {
        client.connect("ftp.impro.net", 21, "anonymous", "anonymous");
    }

    @Test
    void testFolders() throws IOException {
        List<String> directories = client.list();
        for (String directory : directories) {
            Assert.assertTrue(client.cwd(directory));
            Assert.assertTrue(client.cwd(".."));
        }
    }

    @Test
    void testCreateFolder() throws IOException {
        String directory = "TestDirectory";
        Assert.assertTrue(client.mkd(directory));
        Assert.assertTrue(client.rmd(directory));
    }

    @AfterClass
    public void tearDown() throws IOException {
        client.disconnect();
    }
}
