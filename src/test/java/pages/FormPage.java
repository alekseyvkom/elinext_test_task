package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class FormPage {
    public By guestFirstName = By.id("guestFirstName");
    public By guestLastName = By.id("guestLastName");
    public By guestPhone = By.id("guestPhone");
    public By guestEmail = By.id("guestEmail");
    public By guestAddressType = By.id("guestAddressType");
    public By guestCountry = By.id("guestCountry");
    public By guestGenericAddress1 = By.id("guestGenericAddress1");
    public By guestGenericCity = By.id("guestGenericCity");
    private By continueBtn = By.linkText("Continue");

    private WebDriver driver;


    public FormPage(WebDriver driver) {
        this.driver = driver;
    }

    public void enterValue(By field, String text) {
        driver.findElement(field).sendKeys(text);
    }

    public void selectValue(By field, String text){
        new Select(driver.findElement(field)).selectByVisibleText(text);
    }
    public TotalPrice clickContinue() {
        driver.findElement(continueBtn).click();
        return new TotalPrice(driver);
}}
