package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HotelsPage {

    private WebDriver driver;
    By filterAvailableOnly = By.id("filterAvailableOnly");
    By hotel = By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[2]/div/div/div/div[5]/div/div[3]/div[2]/form/div/fieldset[1]/div/div[4]/div[2]/div/span[1]/a/span");

    public HotelsPage(WebDriver driver) {
        this.driver = driver;
    }

    public void selectFilterAvailableOnly(){
        driver.findElement(filterAvailableOnly).click();
    }

    public RoomsPage selectHotel(int number){
        driver.findElements(hotel).get(number + 1).click();
        return new RoomsPage(driver);
    }
}
