package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class MainPage {

    private By advancedSearch = By.className("toggle_form");
    public By numberOfRooms = By.id("numberOfRooms");
    public By roomsAdults = By.id("room1Adults");
    public By roomChildren = By.id("room1Children");
    private By hotelSearchOneBox = By.id("hotelSearchOneBox");
    public By checkin = By.id("checkin");
    public By checkout = By.id("checkout");
    private By go = By.linkText("GO");
    private WebDriver driver;

    public MainPage(WebDriver driver) {
        this.driver = driver;
    }

    public void  clickAdvancedSearch(){
        driver.findElement(advancedSearch).click();
    }

    public void selectNumber(By field, String number){
        new Select(driver.findElement(field)).selectByVisibleText(number);
    }

    public void selectHotel(String hotel){
        WebElement field = driver.findElement(hotelSearchOneBox);
        field.clear();
        field.sendKeys(hotel);
    }

    public void selectDate(By field, String date){
        WebElement checkIn = driver.findElement(field);
        checkIn.clear();
        checkIn.sendKeys(date);
    }

    public HotelsPage clickGo(){
        driver.findElement(go).click();
        return new HotelsPage(driver);
    }
}
//    public HotelsPage clickGo(){
//        driver.findElement(go).click();
//        return new HotelsPage(driver);
