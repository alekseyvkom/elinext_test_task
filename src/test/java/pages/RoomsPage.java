package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RoomsPage {

    private WebDriver driver;

    public RoomsPage(WebDriver driver) {
        this.driver = driver;
    }

    public FormPage selectRoomWithMaxPrice() {
        List<WebElement> rooms = driver.findElements(By.xpath("/html/body/div[2]/div/div/div/div/div[2]/div/div[2]/div"));
        List<Integer> values = new ArrayList<>();
        for (WebElement room : rooms) {
            String priceString = room.getText();
            int priceInt = Integer.parseInt(priceString.replaceAll("\\$", ""));
            values.add(priceInt);
        }
        String maxPrice = "$" + Collections.max(values);
        String maxPriceXPathFor = "//*[contains(text(), '" + maxPrice + "')]";
        WebElement maxPriceElement = driver.findElement(By.xpath(maxPriceXPathFor));
        maxPriceElement.click();
        
        return new FormPage(driver);
    }
}
