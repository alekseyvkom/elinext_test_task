package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class TotalPrice {
    private WebDriver driver;
    public By hotelName = By.linkText("popup hasDigitalKey property-home-link");
    public By hotelAddress = By.className("adr");
    public By totalPrice = By.className("total-dollars");

    public TotalPrice(WebDriver driver) {
        this.driver = driver;
    }

    public String getText(By field) {
        return driver.findElement(field).getText();
    }
}
